package facci.pm.ta2.poo.pra1;
// CATAGUA CHAVEZ RODDY JAVIER
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity{

    TextView precio;
    ImageView imagen;
    TextView descripcion1;
    TextView Nombre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");
        precio =(TextView)findViewById(R.id.pre);
        imagen =(ImageView) findViewById(R.id.thumbnail);
        descripcion1 =(TextView)findViewById(R.id.des);
        Nombre =(TextView)findViewById(R.id.nom);
        // INICIO - CODE6
        final DataQuery query = DataQuery.get("item");
        //Pregunta 3.3 y 3.4
        //  con (getIntent().getExtras().getString("prueba")
        query.getInBackground(getIntent().getExtras().getString("tarea2"), new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    //accedo al objeto
                    String name = (String) object.get("name");
                    //Relleno
                    Nombre.setText(name);
                    //accedo al objeto
                    String des = (String) object.get("description");
                    //Relleno
                    descripcion1.setText(des);
                    //accedo al objeto
                    String pre = (String) object.get("price");
                    //Relleno
                    precio.setText(pre);
                    //accedo al objeto
                    Bitmap bitmap = (Bitmap) object.get("image");
                     //Relleno
                    imagen.setImageBitmap(bitmap);
                }else {
                } }
        });
        // FIN - CODE6
    }
}




